<?php

/**
 * Menu callback for the settings form.
 */
/**
 * Implementation of hook_form_alter()
 */

function voucher_field_settings_form() {

    global $base_url;
    $form['voucher_field'] = array(
		'#type' => 'fieldset',
		'#title' => t('Voucher Field Settings'),
		'#collapsible' => TRUE,
	);

	$form['voucher_field']['voucher_field_subject'] = array(
	'#type' => 'textfield',
	'#title' => t('Subject'),
	'#default_value' => variable_get('voucher_field_subject', t("You've been invited")),
	'#size' => 40,
	'#maxlength' => 64,
	'#description' => t('Type the subject of the invitation email.'),
	'#required' => TRUE,
	);

	$form['voucher_field']['voucher_field_mail_template'] = array(
	'#type' => 'textarea',
	'#title' => t('Mail template'),
	'#default_value' => theme_invitation_template(),
	'#required' => TRUE,
	'#rows' =>10,
	'#description' => t('Use the following placeholders: @site, @homepage, @join_link, @message, @inviter, @code, @expired.'),
	);

	$form['voucher_field_register_URL'] = array(
	'#type' => 'textfield',
	'#title' => t('Registration page URL'),
	'#default_value' => variable_get('voucher_field_register_URL', $base_url),
	'#required' => FALSE,
	'#description' => t('Place here your \'Create account page\' URL'),
	);

    $form['#validate'] = array('voucher_field_settings_form_validate');

    return system_settings_form($form); 
}
 
/**
 * Form API callback to validate the upload settings form.
 */
function voucher_field_settings_form_validate($form, &$form_state) {
	$values = &$form_state['values'];
	// if something then form_set_error('', t('Some error'));
} 

